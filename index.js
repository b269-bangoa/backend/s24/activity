//console.log("Hi");


// FIRST OUTPUT
function getCube() { 
    num = prompt("Enter number: ");
    cubeOfANumber = num ** 3;
    return cubeOfANumber;
} 

//getCube();
//console.log(`The cube of ${num} is ${cubeOfANumber}`);


//SECOND OUTPUT 
const address = ["258 Washington Ave", "Northwest", " California 90011"];

const [street, district, state] = address;

console.log(`I live at ${street} ${district}, ${state}`);

//THIRD OUTPUT
const animal = {
    name: "Lolong",
    group: "saltwater crocodile",
    weight: "1075 kgs",
    length: "20 ft 3 in"
}

console.log(`${animal.name} was a ${animal.group}. He weighed at ${animal.weight} with a measurement of ${animal.length}.`);

//FOURTH OUTPUT
numbers = [1,2,3,4,5];

numbers.forEach((number) => {
    console.log(number);
})

//FIFTH OUTPUT
const sum = numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue)

console.log(sum)

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);